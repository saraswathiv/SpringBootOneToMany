package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Created by IT on 10/5/2017.
 */
@RestController
@RequestMapping("/roster")
public class rostercontroller {
    @Autowired
    private rosterrepository rosterRepo;

    public static final Logger logger = LoggerFactory.getLogger(rostercontroller.class);

    @RequestMapping(method = RequestMethod.GET,value="/skills")
     public ResponseEntity<Collection<Roster>> getrostersts() {
        //return rosterRepo.findAll();
        return new ResponseEntity<>(rosterRepo.findAll(), HttpStatus.OK);
        }

   @RequestMapping(method = RequestMethod.GET)
   // public Collection<Roster> getroster() {
   public void retrieveEmployee(){

        // fetch all students

        logger.info("============ Fetch all roster  ============");

       // return rosterRepo.findAll();

       List<Roster> rosterList = rosterRepo.findAll();

      // List<Roster> rosterList = rosterRepo.findById(4445);

        // Displaying the Employee details
        for (Roster rost : rosterList) {
            System.out.println("*** Roster Details ***");

            System.out.println("Roster Id   : " + rost.getId());
            System.out.println("Trip type : " + rost.getTRIP_TYPE());
            System.out.println( "Start Location :"+rost.getSTART_LOCATION());
            System.out.println("End Location :"+rost.getEND_LOCATION());

            System.out.println("*** Employee Address Details ***");
            Set<RosterPassengers> rosterpassenger = rost.getSkills();
            for (RosterPassengers rosterdetails : rosterpassenger)
            {
                System.out.println("Employee ID  : " + rosterdetails.getEMPLOYEE_ID());
                System.out.println("Estimate start time    : " + rosterdetails.getESTIMATE_START_TIME());
                System.out.println("Roster passegner status   : " + rosterdetails.getROSTER_PASSENGER_STATUS());
            }
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public void findrosterid(@PathVariable("id") Integer id) {
        // fetch all students

        logger.info("============ Fetch all roster  ============");

        // return rosterRepo.findAll();

      //  List<Roster> rosterList = rosterRepo.findById(id);

        List<Roster> rosterList = rosterRepo.findByTitle(id);



        // List<Roster> rosterList = rosterRepo.findById(4445);

        // Displaying the Employee details
        for (Roster rost : rosterList) {
            System.out.println("*** Roster Details ***");

            System.out.println("Roster Id   : " + rost.getId());
            System.out.println("Trip type : " + rost.getTRIP_TYPE());
            System.out.println( "Start Location :"+rost.getSTART_LOCATION());
            System.out.println("End Location :"+rost.getEND_LOCATION());

            System.out.println("*** Employee Address Details ***");
            Set<RosterPassengers> rosterpassenger = rost.getSkills();
            for (RosterPassengers rosterdetails : rosterpassenger)
            {
                System.out.println("Employee ID  : " + rosterdetails.getEMPLOYEE_ID());
                System.out.println("Estimate start time    : " + rosterdetails.getESTIMATE_START_TIME());
                System.out.println("Roster passegner status   : " + rosterdetails.getROSTER_PASSENGER_STATUS());
            }
        }
    }

    @RequestMapping(value = "/passenger", method = RequestMethod.GET)
    public String getrosterpassenger()
    {
        String ret="";
        for (Roster bookCategory : rosterRepo.findAll()) {
            logger.info(bookCategory.toString());
        }

        return ret;
    }
}
