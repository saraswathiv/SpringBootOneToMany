package com.example.demo.booking.details;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by IT on 10/9/2017.
 */
@Repository
public interface bookingrepository extends CrudRepository<bookemployee, Integer> {

    List<bookemployee> findById(Integer id);


}
