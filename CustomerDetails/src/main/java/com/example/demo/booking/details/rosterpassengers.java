package com.example.demo.booking.details;

import javax.persistence.*;

/**
 * Created by IT on 10/5/2017.
 */
@Entity
@Table(name="roster_passengers")
public class rosterpassengers {

    private Integer id;
    private String EMPLOYEE_ID;
    private String ESTIMATE_START_TIME;
    private Integer ROSTER_PASSENGER_STATUS;
    private Integer ACTIVE;

   // private bookemployee emp;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ROSTER_PASSENGER_ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getESTIMATE_START_TIME() {
        return ESTIMATE_START_TIME;
    }

    public void setESTIMATE_START_TIME(String ESTIMATE_START_TIME) {
        this.ESTIMATE_START_TIME = ESTIMATE_START_TIME;
    }

    public Integer getROSTER_PASSENGER_STATUS() {
        return ROSTER_PASSENGER_STATUS;
    }

    public void setROSTER_PASSENGER_STATUS(Integer ROSTER_PASSENGER_STATUS) {
        this.ROSTER_PASSENGER_STATUS = ROSTER_PASSENGER_STATUS;
    }

  //  @ManyToOne(cascade= CascadeType.ALL)
  //  @JoinColumn(name = "EMPLOYEE_ID")
  /*  public bookemployee getEmp() {
        return emp;
    }

    public void setEmp(bookemployee emp) {
        this.emp = emp;
    }*/

    public Integer getACTIVE() {
        return ACTIVE;
    }

    public void setACTIVE(Integer ACTIVE) {
        this.ACTIVE = ACTIVE;
    }

    public String getEMPLOYEE_ID() {
        return EMPLOYEE_ID;
    }

    public void setEMPLOYEE_ID(String EMPLOYEE_ID) {
        this.EMPLOYEE_ID = EMPLOYEE_ID;
    }
}
