package com.example.demo.booking.details;

import com.example.demo.*;
import com.example.demo.RosterPassengers;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by IT on 10/3/2017.
 */
@Entity
@Table(name = "employees")
public class bookemployee {
    private Integer id;
    private String name;
    private String mobile;
    private String address;
    private String EMPLOYEES_ID;
   // private Set<rosterpassengers> booking;
   private rosterpassengers booking;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

   // @OneToMany(fetch = FetchType.LAZY,mappedBy="emp",cascade = CascadeType.ALL)
   /* @JsonBackReference
    public Set<rosterpassengers> getBooking() {
        return booking;
    }

    public void setBooking(Set<rosterpassengers> booking) {
        this.booking = booking;
    }*/


    public String getEMPLOYEES_ID() {
        return EMPLOYEES_ID;
    }

    public void setEMPLOYEES_ID(String EMPLOYEES_ID) {
        this.EMPLOYEES_ID = EMPLOYEES_ID;
    }

    /*public rosterpassengers getBooking() {
        return booking;
    }

    public void setBooking(rosterpassengers booking) {
        this.booking = booking;
    }*/
}
