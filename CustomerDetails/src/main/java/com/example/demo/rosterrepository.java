package com.example.demo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collection;
import java.util.List;

/**
 * Created by IT on 10/5/2017.
 */
@Repository
public interface rosterrepository extends CrudRepository<Roster, Integer> {

    List<Roster> findAll();

   // @Query(nativeQuery=true)

    //here single condition in where clause in single table
    List<Roster> findById(Integer id);


    //here multiple condition in where clause in single table  //// + "JOIN FETCH  g.skills as s "
    @Query("SELECT g FROM Roster as g "
            +"INNER JOIN g.skills as s "
            + "WHERE g.id = ?1 "
           + "AND g.ACTIVE = 1 "

            )
    List<Roster> findByTitle(@Param("rosterid") Integer rosterid);

   /* @Query(value ="SELECT t FROM Roster t WHERE t.id= ?1")
    public List<Roster> findByTitle(Integer ROSTER_ID);*/




}
