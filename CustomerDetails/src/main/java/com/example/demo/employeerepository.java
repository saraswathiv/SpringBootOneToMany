package com.example.demo;



import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;


/**
 * Created by IT on 10/3/2017.
 */

@Repository
public interface employeerepository extends CrudRepository<employee, Long> {

    Collection<employee> findAll();

    employee findById(Integer id);

   // bookemployee findByName(String name);
}