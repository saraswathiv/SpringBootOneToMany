package com.example.demo;

import javax.persistence.*;

/**
 * Created by IT on 10/3/2017.
 */
@Entity
@Table(name = "employees")
public class employee {
    private Integer id;
    private String name;
    private String mobile;
    private String address;
    private String EMPLOYEES_ID;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEMPLOYEES_ID() {
        return EMPLOYEES_ID;
    }

    public void setEMPLOYEES_ID(String EMPLOYEES_ID) {
        this.EMPLOYEES_ID = EMPLOYEES_ID;
    }
}
