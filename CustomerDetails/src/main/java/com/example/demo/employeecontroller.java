package com.example.demo;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletResponse;
import java.util.Collection;
import java.util.List;

/**
 * Created by IT on 10/3/2017.
 */
@RestController
@RequestMapping("/emp")
public class employeecontroller {

    @Autowired
    private employeerepository empRepo;

    public static final Logger logger = LoggerFactory.getLogger(employeecontroller.class);

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Collection<employee>> getemployee(HttpServletResponse response) {
        //return new ResponseEntity<>(empRepo.findAll(), HttpStatus.OK);
       // System.out.println("controller");
       // return new ResponseEntity<employee>empRepo.findAll();
        response.setHeader("Access-Control-Allow-Origin", "*");//* or origin as u prefer
        response.setHeader("Access-Control-Allow-Credentials", "true");
        return new ResponseEntity(empRepo.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getUser(@PathVariable("id") Integer id,HttpServletResponse response) {
        logger.info("Fetching User with id {}", id);
        employee user = empRepo.findById(id);
        if (user == null) {
            logger.error("User with id {} not found.", id);
        }
      //  response.addHeader("Access-Control-Allow-Origin","http://localhost:3000/");
        response.setHeader("Access-Control-Allow-Origin", "*");//* or origin as u prefer
        response.setHeader("Access-Control-Allow-Credentials", "true");
        return new ResponseEntity<employee>(user, HttpStatus.OK);
    }


}
