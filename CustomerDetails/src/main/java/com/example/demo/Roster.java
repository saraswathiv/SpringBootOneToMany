package com.example.demo;

import javax.persistence.*;
import java.awt.print.Book;
import java.util.Set;

/**
 * Created by IT on 10/5/2017.
 */
@Entity
@Table(name="rosters")
public class Roster {
    private Integer id;
    private String TRIP_TYPE;
    private String START_LOCATION;
    private String END_LOCATION;
    private Integer CAB_ID;
    private Integer ROSTER_STATUS;
    private Integer ACTIVE;
    private Set<RosterPassengers> skills;


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ROSTER_ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTRIP_TYPE() {
        return TRIP_TYPE;
    }

    public void setTRIP_TYPE(String TRIP_TYPE) {
        this.TRIP_TYPE = TRIP_TYPE;
    }

    public String getSTART_LOCATION() {
        return START_LOCATION;
    }

    public void setSTART_LOCATION(String START_LOCATION) {
        this.START_LOCATION = START_LOCATION;
    }

    public String getEND_LOCATION() {
        return END_LOCATION;
    }

    public void setEND_LOCATION(String END_LOCATION) {
        this.END_LOCATION = END_LOCATION;
    }

    public Integer getCAB_ID() {
        return CAB_ID;
    }

    public void setCAB_ID(Integer CAB_ID) {
        this.CAB_ID = CAB_ID;
    }

    public Integer getROSTER_STATUS() {
        return ROSTER_STATUS;
    }

    public void setROSTER_STATUS(Integer ROSTER_STATUS) {
        this.ROSTER_STATUS = ROSTER_STATUS;
    }

    public Integer getACTIVE() {
        return ACTIVE;
    }

    public void setACTIVE(Integer ACTIVE) {
        this.ACTIVE = ACTIVE;
    }

    //@OneToMany(mappedBy = "roster")
    // @OneToOne(mappedBy = "roster")
    // @OneToOne
    //  @JoinColumn(name = "ROSTER_ID")

    @OneToMany(fetch = FetchType.EAGER,mappedBy="roster",cascade = CascadeType.ALL)
    public Set<RosterPassengers> getSkills() {
        return skills;
    }

    public void setSkills(Set<RosterPassengers> skills) {
        this.skills = skills;
    }

    @Override
    public String toString() {
        String result = String.format(
                "Roster[id=%d, TRIP_TYPE='%s']%n",
                id, TRIP_TYPE);
        if (skills != null) {
            for(RosterPassengers book : skills) {
                result += String.format(
                        "RosterPassenger[id=%d, name='%s']%n",
                        book.getId(), book.getEMPLOYEE_ID());
            }
        }

        return result;
    }



}
