package com.example.demo;

import javax.persistence.*;

/**
 * Created by IT on 10/5/2017.
 */
@Entity
@Table(name="locations")
public class locations {

    private Integer LOCATION_ID;
    private String LOCATION_NAME;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getLOCATION_ID() {
        return LOCATION_ID;
    }

    public void setLOCATION_ID(Integer LOCATION_ID) {
        this.LOCATION_ID = LOCATION_ID;
    }

    public String getLOCATION_NAME() {
        return LOCATION_NAME;
    }

    public void setLOCATION_NAME(String LOCATION_NAME) {
        this.LOCATION_NAME = LOCATION_NAME;
    }
}
