package com.example.demo.model;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;

/**
 * Created by IT on 9/14/2017.
 */
@Entity
@Table(name = "driver")
public class Driver {


    public Integer id;
    public String fname;
    public String lname;
    public String mobileno;
    public String address;

    private Vehicle veh;

    public Driver()
    {

    }
    public Driver(String fname){
        this.fname = fname;
    }

    public Driver(String fname, Vehicle veh){
        this.fname = fname;
        this.veh = veh;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "vehicle_id")
    public Vehicle getVeh() {
        return veh;
    }

    public void setVeh(Vehicle veh) {
        this.veh = veh;
    }

    @Override
    public String toString() {
        return String.format(
                "Book[id=%d, name='%s', vehno='%d']",
                id, fname, veh.getVehno());
    }
}
