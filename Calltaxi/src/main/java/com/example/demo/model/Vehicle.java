package com.example.demo.model;
import javax.persistence.*;

/**
 * Created by IT on 9/14/2017.
 */
@Entity
@Table(name = "vehicle")
public class Vehicle {

    private Integer vehid;
    private String vehno;
    private Integer capacity;
    private String vehmodel;
    private Driver driver;

    public Vehicle()
    {

    }

    public Vehicle(String vehno){
        this.vehno = vehno;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }


    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    public Integer getVehid() {
        return vehid;
    }

    public void setVehid(Integer vehid) {
        this.vehid = vehid;
    }

    public String getVehno() {
        return vehno;
    }

    public void setVehno(String vehno) {
        this.vehno = vehno;
    }

    public String getVehmodel() {
        return vehmodel;
    }

    public void setVehmodel(String vehmodel) {
        this.vehmodel = vehmodel;
    }

    @OneToOne(mappedBy = "veh",cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }
}
