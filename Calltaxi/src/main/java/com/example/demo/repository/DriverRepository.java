package com.example.demo.repository;


import com.example.demo.model.Driver;
import com.example.demo.model.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IT on 9/14/2017.
 */
@Repository
public interface DriverRepository extends JpaRepository<Driver, Integer> {
}
