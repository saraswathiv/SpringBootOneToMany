package com.example.demo;


import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.demo.model.Driver;
import com.example.demo.model.Vehicle;
import com.example.demo.repository.DriverRepository;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SpringBootApplication
public class CalltaxiApplication  {
	/*private static final Logger logger = LoggerFactory.getLogger(CalltaxiApplication.class);

	@Autowired
	private DriverRepository bookRepository;*/

	public static void main(String[] args) {
		SpringApplication.run(CalltaxiApplication.class, args);
	}

	/*@Override
	public void run(String... strings) throws Exception {
		// save a couple of books
		List<Driver> books = new ArrayList<>();
		books.add(new Driver("Book A", new Vehicle("49")));
		books.add(new Driver("Book B", new Vehicle("59")));
		books.add(new Driver("Book C", new Vehicle("69")));
		bookRepository.save(books);

		// fetch all books
		/*for (Driver book : bookRepository.findAll()) {
			logger.info(book.toString());
		}
	}*/
}
