package com.example.demo.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.transaction.Transactional;

import org.json.JSONArray;
import org.springframework.stereotype.Repository;

import com.example.demo.model.ShuttleStops;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.example.demo.model.RouteStopsResponse;
import com.example.demo.model.ShuttleRouteStops;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Transactional
@Repository
public class ShuttleRouteStopsDao {
	
	@PersistenceContext
	private EntityManager em;
	
	private static ObjectMapper mapper = new ObjectMapper(); 

	public List<ShuttleStops>getAll() {
		    return em.createQuery("SELECT p FROM ShuttleStops p", ShuttleStops.class).getResultList();
	}
	
	public List<RouteStopsResponse> getRouteStops(Integer stop_id)
	{
		Integer route=0;
		List myList=new ArrayList();
		List ll=new ArrayList();
		
		List i=em.createQuery("SELECT r.route_ID FROM ShuttleRouteStops r WHERE r.stop_ID="+stop_id+" ").getResultList();		
		for (Object temp : i) {			
			route=(Integer) temp;
	    }		
		
		Query q= em.createQuery("SELECT p.id as id, p.stops_Name as stopname,n.route_ID as routeid FROM ShuttleStops p,ShuttleRouteStops n"
		        + " where p.id = n.stop_ID and n.route_ID="+route+" and n.stop_ID > "+stop_id+"  order by p.id"
		       );
		 myList = q.getResultList(); 
			
		 List<RouteStopsResponse> personList = new ArrayList<RouteStopsResponse>();
		 
		
		 
		 for(int j = 0; j < myList.size(); j++){
			 
			// RouteStopsResponse person = new RouteStopsResponse(myList.get(j));
			// person.setId(myList.get(j));
			// myList.get(j);
				RouteStopsResponse person = new RouteStopsResponse(1,1,"sdgds");	
				
			   personList.add(person); // adding each person object to the list.
			}       
        String json = new Gson().toJson(personList);   
        System.out.println("json "+json);
		//return json;*/
		
		return myList;
		
	}
	
	
	//criteria query
	public List<ShuttleStops> getDropLocation(Integer stop_id) {
		
		//cb.gt--greater than
		//cb.groupBy(c.get("currency"));
		//cb.having(cb.gt(cb.count(c), 1))		 
		 /* CriteriaBuilder cb = em.getCriteriaBuilder();		 
		  CriteriaQuery<ShuttleStops> q = cb.createQuery(ShuttleStops.class);
		  Root<ShuttleStops> c = q.from(ShuttleStops.class);		 
		  q.select(c).where(cb.equal(c.get("id"), 5));
		  TypedQuery<ShuttleStops> query = em.createQuery(q);
		  List<ShuttleStops> results = query.getResultList();*/
		Integer route=0;
		List i=em.createQuery("SELECT r.route_ID FROM ShuttleRouteStops r WHERE r.stop_ID="+stop_id+" ").getResultList();		
		for (Object temp : i) {			
			route=(Integer) temp;
	    }
		 
		 CriteriaBuilder cb = em.getCriteriaBuilder();
		 CriteriaQuery<Object[]> criteriaQuery = cb.createQuery(Object[].class);
         Root<ShuttleStops> empRoot = criteriaQuery.from(ShuttleStops.class); 
		 Root<ShuttleRouteStops> deptRoot = criteriaQuery.from(ShuttleRouteStops.class);
         criteriaQuery.multiselect(empRoot, deptRoot);
         criteriaQuery.where(cb.equal(empRoot.get("id"), deptRoot.get("stop_ID")),cb.gt(deptRoot.get("stop_ID"), stop_id),cb.equal(deptRoot.get("route_ID"),route));
         criteriaQuery.orderBy(cb.asc(empRoot.get("id")));
		 Query query=em.createQuery(criteriaQuery);
         List list=query.getResultList();	     
		 return list;
    }
	
	
	
}
