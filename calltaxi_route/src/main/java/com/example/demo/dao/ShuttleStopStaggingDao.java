package com.example.demo.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.example.demo.model.ShuttleStops;
import com.example.demo.model.ShuttleTrip;
import com.example.demo.model.ShuttleTripSeat;
import com.fasterxml.jackson.databind.ObjectMapper;

@Transactional
@Repository
public class ShuttleStopStaggingDao {

	@PersistenceContext
	private EntityManager em;
	
	private static ObjectMapper mapper = new ObjectMapper(); 

	public String getSeatsts(Integer pid,String did) {	
		
		int totcapacity=0,tripseat=0,tripid=0;
		int index1=did.indexOf("-");
		String dropid=did.substring(0,index1);
		String routeid=did.substring(index1+1);
		
		int d_id=Integer.parseInt(dropid);
		int r_id=Integer.parseInt(routeid);
		
		String ret="";		
	    List<ShuttleTrip> tripList= em.createQuery("SELECT  p FROM ShuttleTrip p WHERE p.schedule_Route_ID="+r_id+" AND p.schedule_Date='2017-03-10' AND p.schedule_Time='07:30:00' ", ShuttleTrip.class).getResultList();
	    for ( ShuttleTrip p : tripList){
	    	totcapacity=p.getTotal_Capacity();
	    	tripid=p.getId();
	    	}
	    List<ShuttleTripSeat> tripseatCnt= em.createQuery("SELECT  s FROM ShuttleTripSeat s WHERE s.trip_ID="+tripid+" and s.seat_Begin_Stop is null",ShuttleTripSeat.class).getResultList();
	    tripseat=tripseatCnt.size();	    
	    if(tripseat==0)
	    {
	    	ret=checkSeats(pid,d_id,tripid);
	    }
	    else
	    {
	    	ret=checkSeats(pid,d_id,tripid);
	    }
	   // System.out.println("ret=="+ret);
	    return ret;
	}
	
	public String checkSeats(Integer pid,Integer did,Integer r_id)
	{
		String ret="";
		List<ShuttleTripSeat> bookList1=new ArrayList<ShuttleTripSeat>();
		List<ShuttleTripSeat> bookList2=new ArrayList<ShuttleTripSeat>();
		Query q= em.createQuery("select ts from ShuttleTripSeat ts where ts.trip_ID="+r_id+" and (ts.seat_End_Stop<="+pid+" or ts.seat_Begin_Stop>="+pid+") and ts.seat_Begin_Stop not between "+pid+" and "+did+" ",ShuttleTripSeat.class );
    	bookList1 = q.getResultList(); 
    	//select * from shuttle_trip_seat where Trip_ID=15 and Seat_End_Stop<=4
    	Query q1= em.createQuery("select ts1 from ShuttleTripSeat ts1 where ts1.trip_ID="+r_id+" and ts1.seat_End_Stop<="+pid+" ",ShuttleTripSeat.class );
    	bookList2 = q1.getResultList(); 
    	System.out.println("count=="+bookList1.size());
    	if(bookList1.size()>0)
    	{
    		System.out.println("a");
    		ret="true";
    	}
    	else if(bookList2.size()>0)
    	{
    		System.out.println("b");
    		ret="true";
    	}
    	else
    	{
    		ret="false";
    	}
		
		return ret;
	}
}
