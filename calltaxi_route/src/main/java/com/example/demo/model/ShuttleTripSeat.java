package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="shuttle_trip_seat")
public class ShuttleTripSeat {

	Integer id;	
	Integer Trip_ID;
	Integer Seat_Number;
	Integer Seat_status;
	Integer Seat_Begin_Stop;
	Integer Seat_End_Stop;
	String Payment_Token;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "Trip_Seat_ID")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getTrip_ID() {
		return Trip_ID;
	}
	public void setTrip_ID(Integer trip_ID) {
		Trip_ID = trip_ID;
	}
	public Integer getSeat_Number() {
		return Seat_Number;
	}
	public void setSeat_Number(Integer seat_Number) {
		Seat_Number = seat_Number;
	}
	public Integer getSeat_status() {
		return Seat_status;
	}
	public void setSeat_status(Integer seat_status) {
		Seat_status = seat_status;
	}
	public Integer getSeat_Begin_Stop() {
		return Seat_Begin_Stop;
	}
	public void setSeat_Begin_Stop(Integer seat_Begin_Stop) {
		Seat_Begin_Stop = seat_Begin_Stop;
	}
	public Integer getSeat_End_Stop() {
		return Seat_End_Stop;
	}
	public void setSeat_End_Stop(Integer seat_End_Stop) {
		Seat_End_Stop = seat_End_Stop;
	}
	public String getPayment_Token() {
		return Payment_Token;
	}
	public void setPayment_Token(String payment_Token) {
		Payment_Token = payment_Token;
	}
}
