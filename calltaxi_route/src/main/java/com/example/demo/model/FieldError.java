package com.example.demo.model;

public class FieldError {

	 private String message;
	 
	    /**
	     * Constructor is required to set the object properties.
	     * @param message the error message for the field
	     */
	    public FieldError(String message) {
	        this.message = message;
	    }
	 
	    /** Gets the error message for the field. */
	    public String getMessage() {
	        return message;
	    }
}
