package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="shuttle_scheduled_route")
public class ShuttleRoutes {

	Integer id;
	String Route_Name;
	String Start_Location;
	String End_Location;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "Scheduled_Route_ID")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRoute_Name() {
		return Route_Name;
	}
	public void setRoute_Name(String route_Name) {
		Route_Name = route_Name;
	}
	public String getStart_Location() {
		return Start_Location;
	}
	public void setStart_Location(String start_Location) {
		Start_Location = start_Location;
	}
	public String getEnd_Location() {
		return End_Location;
	}
	public void setEnd_Location(String end_Location) {
		End_Location = end_Location;
	}
}
