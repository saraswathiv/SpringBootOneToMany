package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="corporate")
public class Corporate {

	Integer id;
	String corp_name;
	String manager_name;
	String email;
	Double contact_number;
	String address;
	String profile_pic;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "corp_id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@NotNull(message = "First name is required")  
	@Size(min=2, max=30)
	public String getCorp_name() {
		return corp_name;
	}
	public void setCorp_name(String corp_name) {
		this.corp_name = corp_name;
	}
	public String getManager_name() {
		return manager_name;
	}
	public void setManager_name(String manager_name) {
		this.manager_name = manager_name;
	}
	
	//@Pattern(regexp = "\\d{3}-\\d{3}-\\d{4}")
	@NotNull(message = "Eamil is required")	
	@Size(min=2, max=10)
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Double getContact_number() {
		return contact_number;
	}
	public void setContact_number(Double contact_number) {
		this.contact_number = contact_number;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getProfile_pic() {
		return profile_pic;
	}
	public void setProfile_pic(String profile_pic) {
		this.profile_pic = profile_pic;
	}
}
