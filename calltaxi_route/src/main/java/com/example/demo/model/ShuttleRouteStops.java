package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="shuttle_route_stops")
public class ShuttleRouteStops {

	Integer id;
	Integer Route_ID;
	Integer Stop_ID;

	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="Route_Stops_ID")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getRoute_ID() {
		return Route_ID;
	}
	public void setRoute_ID(Integer route_ID) {
		Route_ID = route_ID;
	}
	public Integer getStop_ID() {
		return Stop_ID;
	}
	public void setStop_ID(Integer stop_ID) {
		Stop_ID = stop_ID;
	}
}
