package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="shuttle_trip")
public class ShuttleTrip {
	
	Integer id;	
	Integer Schedule_Route_ID;
	Integer Total_Capacity;
	Integer Seats_Used;
	String Schedule_Date;
	String Schedule_Time;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "Trip_ID")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}	
	public Integer getSchedule_Route_ID() {
		return Schedule_Route_ID;
	}
	public void setSchedule_Route_ID(Integer schedule_Route_ID) {
		Schedule_Route_ID = schedule_Route_ID;
	}
	public Integer getTotal_Capacity() {
		return Total_Capacity;
	}
	public void setTotal_Capacity(Integer total_Capacity) {
		Total_Capacity = total_Capacity;
	}
	public Integer getSeats_Used() {
		return Seats_Used;
	}
	public void setSeats_Used(Integer seats_Used) {
		Seats_Used = seats_Used;
	}
	public String getSchedule_Date() {
		return Schedule_Date;
	}
	public void setSchedule_Date(String schedule_Date) {
		Schedule_Date = schedule_Date;
	}
	public String getSchedule_Time() {
		return Schedule_Time;
	}
	public void setSchedule_Time(String schedule_Time) {
		Schedule_Time = schedule_Time;
	}

}
