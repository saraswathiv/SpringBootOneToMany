package com.example.demo.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="shuttle_stops")
public class ShuttleStops {

	Integer id;
	String Stops_Name;
	float Stops_Lat;
	float Stops_Long;	
	
	/*Set<ShuttleRouteStops> routes;
	
	@OneToMany(mappedBy="routes")
	public Set<ShuttleRouteStops> getRoutes() {
		return routes;
	}
	public void setRoutes(Set<ShuttleRouteStops> routes) {
		this.routes = routes;
	}*/
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "Stops_ID")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getStops_Name() {
		return Stops_Name;
	}
	public void setStops_Name(String stops_Name) {
		Stops_Name = stops_Name;
	}
	public float getStops_Lat() {
		return Stops_Lat;
	}
	public void setStops_Lat(float stops_Lat) {
		Stops_Lat = stops_Lat;
	}
	public float getStops_Long() {
		return Stops_Long;
	}
	public void setStops_Long(float stops_Long) {
		Stops_Long = stops_Long;
	}
	
}

