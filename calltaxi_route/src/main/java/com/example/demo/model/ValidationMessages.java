package com.example.demo.model;

import java.util.ArrayList;
import java.util.List;

public class ValidationMessages {

	 /** List of field validation errors. */
    private List<FieldError> fieldErrors = new ArrayList<>();
 
    /** Gets the list of field validation errors. */
    @SuppressWarnings("UnusedDeclaration")
    public List<FieldError> getFieldErrors() {
        return fieldErrors;
    }
 
    /** Adds a field validation error. */
    public void addFieldError(String message) {
        FieldError fieldError = new FieldError(message);
        fieldErrors.add(fieldError);
    }
}
