package com.example.demo.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.ShuttleStops;


public interface ShuttleStopsRepository extends JpaRepository<ShuttleStops, Integer>{

	//List<ShuttleStops> findAllByOrderByStops_NameAsc();
}
