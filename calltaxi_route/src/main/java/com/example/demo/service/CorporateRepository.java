package com.example.demo.service;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Corporate;

public interface CorporateRepository extends JpaRepository<Corporate, Integer>{

}
