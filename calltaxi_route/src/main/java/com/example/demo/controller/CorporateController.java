package com.example.demo.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Corporate;
import com.example.demo.model.ValidationMessages;
import com.example.demo.service.CorporateRepository;

@RestController
@RequestMapping("/calltaxi")
public class CorporateController {
	
	 private MessageSource messageSource;
	 @Autowired
	  private CorporateRepository repository;

	  @RequestMapping(method = RequestMethod.GET)
	  public List<Corporate> findAll() {
	    return repository.findAll();
	  }
	
	  @PostMapping("/addvalues")
	 // @RequestMapping(method = RequestMethod.GET,value="/stops")
	  public ResponseEntity <String> addvalues(@RequestBody Corporate corp,HttpServletResponse response,HttpServletRequest request)	 
	  {
		/*Corporate model = new Corporate();
		model.setAddress(corp.getAddress());
		model.setCorp_name(corp.getCorp_name());
		model.setContact_number(corp.getContact_number());
		model.setEmail(corp.getEmail());
		model.setManager_name(corp.getManager_name());	*/	
	   // model.setCreatedAt(new Date());	
		
		
        return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).build();
	   // return repository.saveAndFlush(model);
	  }
	  
	  @RequestMapping(method=RequestMethod.POST,value="/create")
	  public Corporate create(@Validated @RequestBody Corporate contact) {
	   // return repository.save(contact);
		  
		 try {
    	return repository.save(contact);
		 }
		 catch(Exception e)
		 {
			 throw e;
			//this.handleMethodArgumentNotValidException(e);
		 }
	    
	  }
	  
	  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
	  public Corporate findOne(@PathVariable Integer id) {
	    return repository.findOne(id);
	  }
	  
	  @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	  public Corporate update(@PathVariable Integer id, @RequestBody Corporate corp) {
		Corporate model = repository.findOne(id);
	    if (model != null) {
	      model.setAddress(corp.getAddress());
	      model.setCorp_name(corp.getCorp_name());
	      return repository.saveAndFlush(model);
	    }
	    return null;
	  }
	  
	  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	  public void delete(@PathVariable Integer id) {
	    repository.delete(id);
	  }
	  
	   @ExceptionHandler(MethodArgumentNotValidException.class)
	    @ResponseStatus(HttpStatus.BAD_REQUEST)
	    @ResponseBody
	    public ValidationMessages handleMethodArgumentNotValidException(MethodArgumentNotValidException exception) {
	        ValidationMessages validationMessages = new ValidationMessages();
	        BindingResult result = exception.getBindingResult();
	 
	        // process the field validations
	        for (FieldError fieldError : result.getFieldErrors()) {
	            validationMessages.addFieldError(messageSource.getMessage(fieldError, LocaleContextHolder.getLocale()));
	        }
	 
	        // process the global validations
	        for (ObjectError globalError : result.getGlobalErrors()) {
	            validationMessages.addFieldError(globalError.getDefaultMessage());
	        }
	 
	        return validationMessages;
	    }
	  

}
