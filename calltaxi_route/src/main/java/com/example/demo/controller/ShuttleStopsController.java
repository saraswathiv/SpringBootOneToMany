package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.ShuttleRouteStopsDao;
import com.example.demo.model.RouteStopsResponse;
import com.example.demo.model.ShuttleRouteStops;
import com.example.demo.model.ShuttleStops;
import com.example.demo.service.ShuttleStopsRepository;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/shuttle")
public class ShuttleStopsController {

	private static final int List = 0;
	@Autowired
    private ShuttleRouteStopsDao stopsRepo;   
    @RequestMapping(method = RequestMethod.GET,value="/stops")
    public ResponseEntity<List<ShuttleStops>> getshuttlestops(HttpServletResponse response) {   
        response.setHeader("Access-Control-Allow-Origin", "*");//* or origin as u prefer
        response.setHeader("Access-Control-Allow-Credentials", "true");
        return new ResponseEntity(stopsRepo.getAll(), HttpStatus.OK);
    }
    
    @RequestMapping(method = RequestMethod.GET,value="/route/{id}",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)    
    public List<RouteStopsResponse>  getshuttle(@PathVariable("id") Integer stop_id,HttpServletResponse response) {   
        response.setHeader("Access-Control-Allow-Origin", "*");//* or origin as u prefer
        response.setHeader("Access-Control-Allow-Credentials", "true");
        //return new ResponseEntity(stopsRepo.getRouteStops(stop_id), HttpStatus.OK);
        return stopsRepo.getRouteStops(stop_id);
    }
    
    @RequestMapping(method = RequestMethod.GET,value="/routeall/{id}",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)    
    public List<ShuttleStops>  getdropstoplocation(@PathVariable("id") Integer stop_id,HttpServletResponse response) {   
        response.setHeader("Access-Control-Allow-Origin", "*");//* or origin as u prefer
        response.setHeader("Access-Control-Allow-Credentials", "true");
        //return new ResponseEntity(stopsRepo.getRouteStops(stop_id), HttpStatus.OK);
        return stopsRepo.getDropLocation(stop_id);
    }

}
