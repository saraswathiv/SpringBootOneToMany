package com.example.demo.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.ShuttleRouteStopsDao;
import com.example.demo.dao.ShuttleStopStaggingDao;
import com.example.demo.model.RouteStopsResponse;
import com.example.demo.model.ShuttleStops;
import com.example.demo.model.ShuttleTrip;

@RestController
@RequestMapping("/stops")
public class ShuttleStopStaggingController {

	@Autowired
    private ShuttleStopStaggingDao staggRepo;   
	@RequestMapping(method = RequestMethod.GET,value="/from/{pid}/to/{did}",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	 public String getSeatstsvalue(@PathVariable("pid") Integer pid,@PathVariable("did") String did,HttpServletResponse response) {   
        response.setHeader("Access-Control-Allow-Origin", "*");//* or origin as u prefer
        response.setHeader("Access-Control-Allow-Credentials", "true");
        //return new ResponseEntity(stopsRepo.getRouteStops(stop_id), HttpStatus.OK);
        System.out.println("drop id=="+did);
        return staggRepo.getSeatsts(pid, did);
    }
    
}
