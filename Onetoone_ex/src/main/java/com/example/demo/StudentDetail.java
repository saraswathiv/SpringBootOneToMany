package com.example.demo;

/**
 * Created by IT on 9/18/2017.
 */
import javax.persistence.*;

@Entity
@Table(name = "student_detail")
public class StudentDetail {
    private Integer id;
    private Integer age;
    private String vehno;
    private String vehmodel;
    private Student student;

    public StudentDetail(){

    }

    public StudentDetail(Integer age){
        this.age = age;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "age")
    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @OneToOne(mappedBy = "studentDetail")
    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public String getVehno() {
        return vehno;
    }

    public void setVehno(String vehno) {
        this.vehno = vehno;
    }

    public String getVehmodel() {
        return vehmodel;
    }

    public void setVehmodel(String vehmodel) {
        this.vehmodel = vehmodel;
    }
}
