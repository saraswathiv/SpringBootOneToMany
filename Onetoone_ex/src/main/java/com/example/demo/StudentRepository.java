package com.example.demo;

/**
 * Created by IT on 9/18/2017.
 */
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Integer>{
}
