package com.example.demo;

/**
 * Created by IT on 9/18/2017.
 */
import javax.persistence.*;

@Entity
public class Student {
    private int id;
    private String name;
    private String mobileno;
    private StudentDetail studentDetail;
   // private Tariff tariff;

    public Student(){

    }

    public Student(String name){
        this.name = name;
    }

    public Student(String name, StudentDetail studentDetail){
        this.name = name;
        this.studentDetail = studentDetail;
    }


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "student_detail_id")
    public StudentDetail getStudentDetail() {
        return studentDetail;
    }

    public void setStudentDetail(StudentDetail studentDetail) {
        this.studentDetail = studentDetail;
    }

    @Override
    public String toString() {
        return String.format(
                "student[id=%d, name='%s',mobileno='%s', age='%d',vehno='%s',vehmodel='%s']",
                id, name,mobileno, studentDetail.getAge(),studentDetail.getVehno(),studentDetail.getVehmodel());
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    /*public Tariff getTariff() {
        return tariff;
    }

    public void setTariff(Tariff tariff) {
        this.tariff = tariff;
    }*/
}
