package com.example.demo;

/**
 * Created by IT on 9/18/2017.
 */
import javax.persistence.*;

@Entity
public class Tariff {
    private Integer tariffid;
    private float basecost;
    private float basekm;
    private float excost;
    private float exkm;
    private String tariff_name;
    private String tariff_type;
    private Integer vehid;
   // private Student student;

    public Tariff()
    {

    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getTariffid() {
        return tariffid;
    }

    public void setTariffid(Integer tariffid) {
        this.tariffid = tariffid;
    }

    public float getBasecost() {
        return basecost;
    }

    public void setBasecost(float basecost) {
        this.basecost = basecost;
    }

    public float getBasekm() {
        return basekm;
    }

    public void setBasekm(float basekm) {
        this.basekm = basekm;
    }

    public float getExcost() {
        return excost;
    }

    public void setExcost(float excost) {
        this.excost = excost;
    }

    public float getExkm() {
        return exkm;
    }

    public void setExkm(float exkm) {
        this.exkm = exkm;
    }

    public String getTariff_name() {
        return tariff_name;
    }

    public void setTariff_name(String tariff_name) {
        this.tariff_name = tariff_name;
    }

    public String getTariff_type() {
        return tariff_type;
    }

    public void setTariff_type(String tariff_type) {
        this.tariff_type = tariff_type;
    }

    public Integer getVehid() {
        return vehid;
    }

    public void setVehid(Integer vehid) {
        this.vehid = vehid;
    }
   /* @OneToOne(mappedBy = "tariff")
    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }*/
}


//https://gigsterous.github.io/engineering/2016/09/25/spring-boot-2.html